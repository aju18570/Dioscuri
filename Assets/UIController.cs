﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Tell the system we're going to be using UI controls
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    //Define what image we're dealing with
    public Image Img1;
    public Image Img2;
    public Text InputText;
    public Text OutputText;


    // Start is called before the first frame update
    void Start()
    {
        Img1.enabled = false;
        Img2.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
   

        //checks if the user presses Q
        if (Input.GetKeyDown("q") && Img1.enabled == true)
        {
            //disables the img
            Img1.enabled = false;
            Img2.enabled = false;
        }

       else if (Input.GetKeyDown("q") && Img1.enabled == false)
        {
            Img1.enabled = true;
            Img2.enabled = false;
        }


        if (Input.GetKeyDown("e") && Img2.enabled == false)
        {
            //disables the img
            Img1.enabled = false;
            Img2.enabled = true;
        }

        else if (Input.GetKeyDown("e") && Img2.enabled == true)
        {
            Img1.enabled = false;
            Img2.enabled = false;
        }

        // unlocks the cursor so you can use it bc we locked in player look script
        Cursor.lockState = CursorLockMode.None;

        //displays the user input in the answer text field
        OutputText.text = InputText.text;

        if(InputText.text == "Pyro" || InputText.text == "pyro" )
        {
            OutputText.text = "that is correct. Shawty";
        }
        else if (InputText.text == "")
        {
            OutputText.text = "swag";
        }
        else
        {
            OutputText.text = "Your answer is " + InputText.text + ". Shawty that's wrong.";
        }

    }
}
