﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class orbMovement : MonoBehaviour
{
    public Renderer Colour;

    public Transform Player;

    public Rigidbody Orb;

    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Input.GetKeyDown("e"))

            {
                Debug.Log("Is it bussin?");
                Vector3 OrbPosition = Orb.transform.position;
                Vector3 PlayerPosition = Player.transform.position;

                Vector3 Direction = OrbPosition - PlayerPosition;
                Debug.DrawLine(OrbPosition, OrbPosition+Direction, Color.red, Mathf.Infinity);

                Orb.AddForce(Direction * 5, ForceMode.Impulse);
            }
        }
    }


    
    public void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player") { 
        Debug.Log("Are you headed straight for the floor?");
        Colour.material.SetColor("_Color", Color.yellow );
        }
    }

    //public void OnTriggerStay(Collider other)
    //{
      //  if (other.tag == "Player")
        //{
          //  Debug.Log("Hello");
            //Color randomColour = Random.ColorHSV();
            //Colour.material.SetColor("_Color", randomColour);
        //}
    //}

    public void OnTriggerExit(Collider other)
    {
        Debug.Log("See ya swagger");
        Colour.material.SetColor("_Color", Color.white);
    }
}
